package com.hiper.apirestmensaje;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.hiper.apirestmensaje.controllers.App;

@SpringBootTest(classes = App.class)
public class AppTests {
    
    private MockMvc mockMvc;
    
    @InjectMocks
    private App app;
    
   /*  @MockBean
    private AppService appService;
    */

    @Test
    public void testHolaMundo() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(app).build();
        
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().string("Hola mundo"));
    }
}